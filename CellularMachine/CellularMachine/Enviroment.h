﻿#pragma once
// p r z y k l a d automatu dwuwymiarowego na s i e c i kwadra towe j
// Regule stanow i stan sasiedztwa 9 kwadratow ( t u t a j gw i a z d k i )
// s i e c i kwadra towe j LxL
//
// ∗ ∗ ∗
// ∗ ∗ ∗
// ∗ ∗ ∗
// S tan s i w e zl a srodkowego i w nastepnym kroku czasowym z a l e z y od s tanow
// s k d z i e w i e c i u wezlow
//w kroku poprzedn im k = 1 . . 9 :
//
// s i =suma k =1 . .9 s k
// Regula :
// if ( suma<3) s i=0
// if ( suma>=3 && suma<5) s i=1
// if ( suma>=5) s i=0

#include<iostream>
#include<fstream>
#include<cassert>
#include<vector>

using namespace std;

class Environment {
private:
	vector<int> neighbors;
	int state;
	int L;
	int sum;

public:
	Environment(int L) : L(L) { neighbors = vector<int>(9, 0); };

	int specify_the_state_of_the_environment(vector<int> &neighbors, int number);

	int periodic_conditions(int i);
	void initial_conditions(vector<int> &neighbors, int L);

	void reset_the_grid(vector<int> &neighbors, int L);
	void replace_the_grid(vector<int> &grid_A, vector<int> &grid_B, int L2);

	void save_to_PPM_file(int N, int M, vector<int> &neighbors, int id);

};
