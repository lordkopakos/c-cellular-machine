﻿// p r z y k l a d automatu dwuwymiarowego na s i e c i kwadra towe j
// Regule stanow i stan sasiedztwa 9 kwadratow ( t u t a j gw i a z d k i )
// s i e c i kwadra towe j LxL
//
// ∗ ∗ ∗
// ∗ ∗ ∗
// ∗ ∗ ∗
// S tan s i w e zl a srodkowego i w nastepnym kroku czasowym z a l e z y od s tanow
// s k d z i e w i e c i u wezlow
//w kroku poprzedn im k = 1 . . 9 :
//
// s i =suma k =1 . .9 s k
// Regula :
// if ( suma<3) s i=0
// if ( suma>=3 && suma<5) s i=1
// if ( suma>=5) s i=0

#include<iostream>
#include<fstream>
#include<cassert>
#include<vector>
#include<future>
#include<chrono>

#include"Enviroment.h"

using namespace std;
int main() {
	cout << "Wprowadz wymiar liniowy sieci kwadratowej L = ";
	int L;
	cin >> L;
	int L2 = L*L;
	cout << "Podaj liczbe krokow czasowych ewolucji automatu komorkowego N = ";
	int Nkrokow;
	cin >> Nkrokow;

	auto grid = vector<int>(L2, 0);
	auto second_grid = vector<int>(L2, 0);


	Environment enviroment(L);
	enviroment.initial_conditions(grid, L);
	auto future1 = std::async(&Environment::save_to_PPM_file, &enviroment, L, L, grid, -1);
	auto time_start = chrono::system_clock::now();
	auto duration_start = time_start.time_since_epoch().count();
	for (int t = 0; t<Nkrokow; t++) {
		enviroment.reset_the_grid(second_grid, L);

		for (int n = 0; n<L2; n++) {
			second_grid[n] = enviroment.specify_the_state_of_the_environment(grid, n);
		}
		enviroment.replace_the_grid(grid, second_grid, L2);
		auto future = std::async(&Environment::save_to_PPM_file, &enviroment, L, L, grid, t);
	}

	auto time_end = chrono::system_clock::now();
	auto duration_end = time_end.time_since_epoch().count();

	cout << duration_end - duration_start << endl;
	system("pause");
	return 0;
}